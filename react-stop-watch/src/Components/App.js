import React, { Component } from 'react';
import './App.css';
import Title from './Title/title';
import Countdown from './Countdown/Countdown';
import Controller from './Controller/controller';
import Laps from './Laps/laps';

class App extends Component { 
  
  constructor(props) { 
    super(props); 

    this.state = { 
      time: { 
        min: 0, 
        sec: 0, 
        mili: 0 
      }, 
      laps: [] 
    } 
  } 


  getStart() { 
    this.intervalID = setInterval( () => { 
      let min = this.state.time.min;
      let sec = this.state.time.sec;
      let mili = this.state.time.mili;

      if(mili >= 10) { 
        sec = sec + 1;
        mili = 0;
      } 

      if(sec >= 60) { 
        min = min + 1;
        sec = 0
      } 

      this.setState ({ 
        time: { 
          ...this.state,
          min,
          sec,
          mili: mili + 1
        } 
      }) 
    } , 100); 
  } 
  

  getPause() {
    clearInterval( this.intervalID );
  } 

  getLap() {
    let clickTime = {
      ...this.state.time,
    } 

    this.setState ({ 
      ...this.state,  
      laps: [clickTime, ...this.state.laps] 
    }) 
    
    console.log(this.state.laps);
  } 
  
  getRestart() { 
    this.setState ({ 
      time: { 
        min: 0, 
        sec: 0, 
        mili: 0 
      }, 
      laps: []
    }) 

  }
  






  render() { 
    return ( 
      <div className="App"> 
          <div className="mt-3 container"> 
            <div className="col-sm-8 offset-sm-2"> 
              <Title /> 
            </div> 
          </div> 

          <div className="py-4 container"> 
            <Countdown time={this.state.time}/> 
          </div> 

          <div className="container"> 
            <Controller 
                start= { () => this.getStart() } 
                pause= { () => this.getPause() } 
                restart= { () => this.getRestart() } 
                lap= { () => this.getLap() } 
            /> 
          </div> 

          <div className="container col-sm-8 my-5 d-flex justify-content-center"> 
            <Laps laps={this.state.laps}/> 
          </div> 
      </div> 
    ); 
  } 
} 

export default App;
