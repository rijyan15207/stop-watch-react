import React, { Component } from 'react';

class Controller extends Component { 

    constructor(props) { 
        super(props); 
 
        // object 
        this.state = { 
            start: true, 
            pause: false, 
            lap: false, 
            restart: false 
        } 
    } 


    startClicked() { 
        this.setState ({ 
            ...this.state, 
            start: false, 
            pause: true, 
            lap: true, 
        }) 

        this.props.start();
    } 

    pauseClicked() { 
        this.setState ({ 
            ...this.state, 
            start: true, 
            pause: false, 
            lap: false, 
            restart: true 
        }) 

        this.props.pause();
    } 

    lapClicked() { 
        this.props.lap() 
    } 

    
    restartClicked() { 
        this.setState ({ 
            ...this.state, 
            start: true, 
            pause: false, 
            lap: false, 
            restart: false 
        }) 

        this.props.restart();
    } 



    getController() {
        let output = null;

        if(this.state.start && !this.state.restart) { 
            output = ( 
                <div className="d-flex justify-content-center"> 
                    <button className="btn btn-lg btn-success"
                            onClick= {() => this.startClicked()}> 
                        Start 
                    </button> 
                </div> 
            ) 
        } 

        else if(this.state.pause && this.state.lap) { 
            output = ( 
                <div className="d-flex justify-content-center"> 
                    <button className="btn btn-lg btn-danger"
                            onClick= {() => this.pauseClicked()}>  
                        Pause
                    </button> 
                    <div className="offset-sm-1"></div> 
                    <button className="btn btn-lg btn-warning"
                            onClick= {() => this.lapClicked()}> 
                        Lap
                    </button> 
                </div> 
            ) 
        } 
        else if(this.state.start && this.state.restart) { 
            output = ( 
                <div className="d-flex justify-content-center"> 
                    <button className="btn btn-lg btn-success"
                            onClick= {() => this.startClicked()}>  
                        Start
                    </button> 
                    <div className="offset-sm-1"></div>
                    <button className="btn btn-lg btn-dark"
                            onClick= {() => this.restartClicked()}>  
                        Reset
                    </button> 
                </div> 
            ) 
        } 

        return output; 
    } 

    render() { 
        return this.getController();
    } 
} 

export default Controller 