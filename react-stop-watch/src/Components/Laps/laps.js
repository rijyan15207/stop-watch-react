import React from 'react'

const Laps = (props) => { 
    return ( 
        <div> 
            <ul className="list-group list-group-flush"> 
                { props.laps.map( (lap, index) => { 
                    return ( 
                        <li key={ index } className="list-group-item"> 
                            <h3> <span className="text-dark">Minute: </span> 
                            <span className="text-danger"> 
                                { lap.min < 10 ? `0${lap.min}` : lap.min } 
                            </span> 

                            <span className="text-dark pl-5">Second: </span> 
                            <span className="text-danger"> 
                                { lap.sec < 10 ? `0${lap.sec}` : lap.sec } 
                            </span> 

                            <span className="text-dark pl-5">Mili-Second: </span> 
                            <span className="text-danger"> 
                                { lap.mili < 10 ? `0${lap.mili}` : lap.mili } 
                            </span> </h3> 
                        </li> 
                    ) 
                }) } 
            </ul> 
        </div> 
    ) 
} 

export default Laps 

