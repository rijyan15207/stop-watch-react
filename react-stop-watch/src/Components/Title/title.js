import React, { Component } from 'react';
import './title.css';

class Title extends Component { 

    constructor(props) {
        super(props);

        // object
        this.state = {
            title: 'This is a dummy text',
            flag: false
        }
    }


    edit() { 
        this.setState({ 
            ...this.state, 
            flag: true 
        }) 
    } 

    change(event) { 

        this.setState ({ 
            ...this.state,
            title: event.target.value
        }) 
    } 


    keyPress(event) {
        if(event.key === 'Enter') {
            this.setState({
                ...this.state,
                flag: false
            }) 
        } 
    } 

    blur(event) { 
        this.setState({
            ...this.state,
            flag: false
        }) 
    } 



    // --------------------------------------------------------------
    // --------------------------------------------------------------
    // --------------------------------------------------------------



    render() { 

        let output = null; 
        
        if(this.state.flag) { 
            // true -- input box
            output = ( 
                <div className="title"> 
                    <input value= { this.state.title } className="form-control"
                    onChange= { (event) => this.change(event) } 
                    onKeyPress= { (event) => this.keyPress(event) } 
                    onBlur= { (event) => this.blur(event) } 
                    /> 
                </div> 
            ) 
        } 
        else { 
            // false -- title show 
            output = ( 
                <div className="d-flex title"> 
                    <h1 className="display-4"> {this.state.title} </h1> 
                    <span onClick={ () => this.edit() } className="ml-auto icon"> 
                        <i className="fas fa-pencil-alt"></i> 
                    </span> 
                </div> 
            ) 
        } 
        
        

        return ( 
            <div> 
                { output } 
            </div> 
        ) 
    } 
   
} 


export default Title








// ////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////

